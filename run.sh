#!/usr/bin/env sh

set -eu

# Create a boot disk with a basic hello world as default boot target
mkdir -p build/disk/efi/boot
cp build/main.efi build/disk/efi/boot/bootx64.efi
cp build/main.efi build/disk/
cp build/pong.efi build/disk/

# Run QEMU with UEFI firmware and a local folder mounted as a FAT filesystem
qemu-system-x86_64 -bios /usr/share/qemu/OVMF.fd -hdb fat:rw:build/disk
