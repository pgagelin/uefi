#!/usr/bin/env sh

set -eu

rm -rf build
mkdir -p build

clang -I/usr/include/efi -target x86_64-unknown-windows -ffreestanding -fshort-wchar -mno-red-zone -c src/main.c -o build/main.o
clang -target x86_64-unknown-windows -nostdlib -Wl,-entry:efi_main -Wl,-subsystem:efi_application -fuse-ld=lld-link -o build/main.efi build/main.o

clang -I/usr/include/efi -target x86_64-unknown-windows -ffreestanding -fshort-wchar -mno-red-zone -c src/pong.c -o build/pong.o
clang -target x86_64-unknown-windows -nostdlib -Wl,-entry:efi_main -Wl,-subsystem:efi_application -fuse-ld=lld-link -o build/pong.efi build/pong.o
