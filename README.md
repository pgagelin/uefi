# UEFI

This directory hosts an example of how to build and run a UEFI application

It takes UEFI headers from [GNU-EFI](https://wiki.osdev.org/GNU-EFI) and builds them with [Clang](https://clang.llvm.org/) and [LLD](https://lld.llvm.org/)

## Build and run

One can use the helper scripts:

```sh
# Builds a minimal hello world and a Pong game
./build.sh

# Runs QEMU with a UEFI firmware
#
# - let it run until it displays the firmware configuration menu
# - select "Boot Maintenance Manager" > "Boot From File" > "QEMU VVFAT" > "pong.efi"
#
# You can also run "main.efi" this way but it runs only for some milliseconds. Use the UEFI Shell to see the output
#
./run.sh
```

## Dependencies

The following packages are required:

- `clang`
- `lld`
- `gnu-efi`
- `qemu-system-x86`
